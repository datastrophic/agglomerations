osmconvert central-america-latest.osm.pbf --all-to-nodes --max-objects=3500000000 -o=central.o5m
osmfilter central.o5m --keep="building=" --ignore-dependencies --drop-author --out-o5m -o=buildings.o5m
osmconvert buildings.o5m -o=buildings.pbf

