import esy.osm.pbf
import pygeohash as pgh
import json
from collections import defaultdict
import random
import csv
import math

osm = esy.osm.pbf.File('buildings.pbf')
print("read!")

tile_total = defaultdict(lambda: 0)

def distance(origin, destination):
    lat1 = origin['lat']
    lon1 = origin['lon']
    lat2 = destination['lat']
    lon2 = destination['lon']

    radius = 6371  # km

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon / 2) * math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c

    return d

def make_tile_key(node):
    (lon, lat) = node.lonlat
    return pgh.encode(lat, lon, precision=5)

def tile_to_region_key(tile_key):
    if tile_key[-1] in "uvyzstwxkmqrhjnp":
        return tile_key[:-1] + "<"
    else:
        return tile_key[:-1] + ">"

i = 0

for entry in osm:
    tile_total[make_tile_key(entry)] += 1
    i += 1
    if i % 1000 == 0:
        print(str(i))

print("TILES")
print(len(tile_total))

region_total = defaultdict(lambda: 0)
region_max = defaultdict(lambda: 0)
max_tile = dict()

for tile in tile_total.items():
    (tile_key, total) = tile
    region_key = tile_to_region_key(tile_key)
    region_total[region_key] += total
    if region_max[region_key] < total:
        region_max[region_key] = total
        max_tile[region_key] = tile_key

print("REGIONS")
print(len(region_max))

agglos = set()
for region in region_total.items():
    (region_key, total) = region
    if total < 120: continue
    if region_max[region_key] < 35: continue
    agglos.add(max_tile[region_key])

print("AGGLOS!")
print(len(agglos))

i = 0

output = defaultdict(lambda: [])

def object_type_from_id(id):
    if id < 1000000000000000:
        return "n"
    elif id < 2000000000000000:
        return "w"
    elif id < 3000000000000000:
        return "r"
    else:
        raise Exception('Bad id: {}'.format(id))

def strip_type_offset(id):
    return id % 1000000000000000

for entry in osm:
    key = make_tile_key(entry)

    if key in agglos:
        curr_len = len(output[key])
        if curr_len < 16:
            total = tile_total[key]
            if random.randint(0, total-1) < 16 - curr_len:
                (lon, lat) = entry.lonlat
                output[key].append({
                    'id': strip_type_offset(entry.id),
                    'lat': round(lat, 6),
                    'lon': round(lon, 6),
                    'type': object_type_from_id(entry.id)
                })
            tile_total[key] -= 1

    i += 1
    if i % 1000 == 0:
        print(str(i))


averages = dict()

for tile in output.items():
    (tile_key, nodes) = tile
    lat_sum = 0
    lon_sum = 0
    for node in nodes:
        lat_sum += node['lat']
        lon_sum += node['lon']
    averages[tile_key] = {
                    'lat': round(lat_sum / 16, 6),
                    'lon': round(lon_sum / 16, 6),
                }

for tile in output.items():
    (tile_key, nodes) = tile
    average = averages[tile_key]
    center_node = min(nodes, key=lambda node: distance(node, average))
    nodes.sort(key=lambda node: distance(node, center_node))

with open('data.txt', 'w') as outfile:
    json.dump(output, outfile)

with open('averages.txt', 'w') as outfile:
    json.dump(averages, outfile)

with open('data.csv', mode='w') as employee_file:
    csv_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for average in averages.items():
        (tile_key, latlon) = average
        csv_writer.writerow([latlon['lat'], latlon['lon'], tile_key])
